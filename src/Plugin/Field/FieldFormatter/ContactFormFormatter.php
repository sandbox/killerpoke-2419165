<?php

/**
 * @file
 * Contains \Drupal\contact_form_formatter\Plugin\field\formatter\ContactFormFormatter.
 */

namespace Drupal\contact_form_formatter\Plugin\Field\FieldFormatter;

use Drupal\contact\Controller\ContactController;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin for responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "contact_form_formatter",
 *   label = @Translation("Contact Form Formatter"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class ContactFormFormatter extends EntityReferenceFormatterBase {
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $lang) {

    $elements = array();

    // create contact controller instance to create the form

    $controller = new ContactController(\Drupal::service('renderer'));

    foreach ($this->getEntitiesToView($items, $lang) as $delta => $entity) {

        $message = \Drupal::service('entity.manager')
          ->getStorage('contact_message')
          ->create(array(
            'contact_form' => $entity->id(),
          ));

        if(!$message->isPersonal()) {
            $elements[$delta] = array(
              '#cache' => array(
                'tags' => array('config:entity_form_display_list'),
              ),
              'form' => $controller->contactSitePage($entity),
            );
        }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return ($field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'contact_form');
  }
}
